local S = minetest.get_translator("aes_portals")
local storage = minetest.get_mod_storage()

local function check_portal_node() end

local BL_ARENAS = arena_lib.mods["block_league"].arenas
local FB_ARENAS = arena_lib.mods["fantasy_brawl"].arenas
local exceptions = {block_league = {}, fantasy_brawl = {}}
local just_checked = {}

if storage:get_string("exceptions") ~= "" then
  exceptions = minetest.deserialize(storage:get_string("exceptions"))
end



minetest.register_globalstep(function(dtime)
  for _, pl in pairs(hub.get_players_in_hub(true)) do
    local p_node_name = minetest.get_node(pl:get_pos()).name
    local pl_name = pl:get_player_name()

    -- se han appena toccato un portale, non controllo per un secondo
    if not just_checked[pl_name] then
      check_portal_node("block_league", "aes_portals:portal_bl", BL_ARENAS, p_node_name, pl_name)
      check_portal_node("fantasy_brawl", "aes_portals:portal_fb", FB_ARENAS, p_node_name, pl_name)
    end
  end
end)



----------------------------------------------
---------------------CORE---------------------
----------------------------------------------

function aes_portals.link(mod, id)
  local mod_ref = arena_lib.mods[mod]
  if not mod_ref or not mod_ref.arenas[id] or not exceptions[mod][id] then return end

  exceptions[mod][id] = nil
  storage:set_string("exceptions", minetest.serialize(exceptions))
end



function aes_portals.unlink(mod, id)
  local mod_ref = arena_lib.mods[mod]
  if not mod_ref or not mod_ref.arenas[id] or exceptions[mod][id] then return end

  exceptions[mod][id] = true
  storage:set_string("exceptions", minetest.serialize(exceptions))
end



function aes_portals.get_linked_arenas(mod)
  if not arena_lib.mods[mod] then return end

  local list = {}
  local arenas

  if mod == "block_league" then
    arenas = BL_ARENAS
  elseif mod == "fantasy_brawl" then
    arenas = FB_ARENAS
  else
    return
  end

  for id, arena in pairs(arenas) do
    if not exceptions[mod][id] then
      list[id] = arena.name
    end
  end

  return list
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function check_portal_node(mod, portal_node, arenas, p_node_name, p_name)
  if p_node_name == portal_node and not arena_lib.is_player_in_queue(p_name, mod) then
    local game_arena, queue_arena, full_arena
    local chosen_p_count = 0
    local is_in_party = parties.is_player_in_party(p_name)

    just_checked[p_name] = true

    minetest.after(1, function()
      just_checked[p_name] = nil
    end)

    -- se è in squadra e non è lə capəgruppo, annulla
    if is_in_party and not parties.is_player_party_leader(p_name) then
      arena_lib.HUD_send_msg("title", p_name, S("Only the party leader can join a game!"), 5)

    -- sennò continua
    else
      local discarded_arenas = {} -- le arene controllate che non soddisfano i requisiti; usate per alleggerire il for in fondo
      local party_size = parties.is_player_in_party(p_name) and #parties.get_party_members(p_name, true) or 1

      for ar_id, arena in pairs(arenas) do
        local is_exception = exceptions[mod][ar_id]
        local p_amount = arena.players_amount
        local teams_amount = #arena.teams
        local max_players = arena.max_players * teams_amount

        -- prima scremata di arene inadatte
        if is_exception or not arena.enabled or arena.in_loading or arena.in_celebration then
          discarded_arenas[ar_id] = true

        -- salvo la prima piena murata in caso non ne trovase di libere
        elseif p_amount == max_players and not full_arena then
          full_arena = arena
          discarded_arenas[ar_id] = true

        -- controllo se è in partita
        elseif arena.in_game then
          if p_amount > chosen_p_count and p_amount < max_players and max_players - p_amount >= party_size then
            if teams_amount > 1 then
              for team_id, _ in pairs(arena.teams) do
                if arena.max_players - arena.players_amount_per_team[team_id] >= party_size then
                  game_arena = arena
                  chosen_p_count = p_amount
                  break
                end
              end

              if not game_arena then
                discarded_arenas[ar_id] = true
              end

            else
              game_arena = arena
              chosen_p_count = p_amount
            end
          else
            discarded_arenas[ar_id] = true
          end

        -- sennò se ha almeno una persona in coda
        elseif p_amount > 0 then
          if p_amount > chosen_p_count and p_amount < max_players and max_players - p_amount >= party_size then
            if teams_amount > 1 then
              for team_id, _ in pairs(arena.teams) do
                if arena.max_players - arena.players_amount_per_team[team_id] >= party_size then
                  queue_arena = arena
                  chosen_p_count = p_amount
                  break
                end
              end

              if not queue_arena then
                discarded_arenas[ar_id] = true
              end

            else
              queue_arena = arena
              chosen_p_count = p_amount
            end
          else
            discarded_arenas[ar_id] = true
          end
        end
      end

      -- se ne ha trovata una in gioco non piena, inserisci
      if game_arena then
        local id = arena_lib.get_arena_by_name(mod, game_arena.name)
        arena_lib.join_arena(mod, p_name, id)

      -- se ce n'è una in coda
      elseif queue_arena then
        arena_lib.join_queue(mod, queue_arena, p_name)
        arena_lib.HUD_send_msg("title", p_name, S("No ongoing arenas found: you've been put into a queue (right-click the portal to leave)"), 5)


      -- se ce n'è una in gioco piena e non si è in gruppo (sennò fa strano che entri solo unə)
      elseif full_arena and party_size == 1 then
        local id = arena_lib.get_arena_by_name(mod, full_arena.name)
        arena_lib.join_arena(mod, p_name, id, true)
        arena_lib.HUD_send_msg("title", p_name, S("Arena full: entering as spectator"), 5)

      -- sennò la prima che trova
      else
        local shuffled_arenas = {}

        for ar_id, _ in pairs(arenas) do
          shuffled_arenas[#shuffled_arenas + 1] = ar_id
        end

        table.shuffle(shuffled_arenas)

        for _, ar_id in ipairs(shuffled_arenas) do
          if not discarded_arenas[ar_id] then
            arena_lib.join_queue(mod, arenas[ar_id], p_name)
            arena_lib.HUD_send_msg("title", p_name, S("No ongoing arenas found: you've been put into a queue (right-click the portal to leave)"), 5)
            return
          end
        end

        -- in caso non ne abbia trovata nessuna (es. tutte disabilitate), avvisa
        arena_lib.HUD_send_msg("title", p_name, S("There are no arenas available at the moment"), 5)
      end
    end
  end
end